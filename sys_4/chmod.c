#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

const char *call_format = "./chmod [uga][-+][xrw] file_path";

int main(int argc, char const *argv[])
{
  // Проверяем кол-во аргументов вызова программы
  if (argc != 3)
  {
    fprintf(stderr,
            "Неправильный вызов программы.\n"
            "Используйте вызов в формате"
            " '%s'\n",
            call_format);
    return EXIT_FAILURE;
  }

  const char* mode = argv[1];
  const char* file_path = argv[2];

  // Проверяем уровень доступа к файлу и читаем текущую маску доступа
  struct stat file_stat;
  if (stat(file_path, &file_stat) == -1)
  {
    fprintf(stderr, "Невозможно прочитать файл '%s'\n", argv[1]);
    return EXIT_FAILURE;
  }

  // Проверяем аргументы вызова программы
  int len = strlen(mode);
  if (len < 2)
  {
    fprintf(stderr, "Неправильный формат модификации прав.\n"
                    "Используйте вызов в формате"
                    " '%s'\n", call_format);
    return EXIT_FAILURE;
  }

  // Делим ключ команды на "-" или "+",
  // то есть ищем индекс в argv[1] где строка делится этим симоволом
  // пополам и используем левую часть как модификатор уровня привилегий
  // (u - юзер, g - группа, a - другие)
  // а правую - как тип привелегий
  // (r - читать, w - писать, x - испольнять/искать)
  char *operation_sep_slice;
  // Для дальшейншего определения добавим is_removing
  int is_removing = 0;
  operation_sep_slice = strchr(mode, '+');
  if (operation_sep_slice == NULL)
  {
    operation_sep_slice = strchr(mode, '-');
    is_removing = 1;
  }

  // Символ +/- не найден в строке - неправильный формат
  if (operation_sep_slice == NULL)
  {
    fprintf(stderr, "Неправильный формат модификации прав "
                    "(нет +/-).\n"
                    "Используйте вызов в формате"
                    " '%s'\n",
            call_format);
    return EXIT_FAILURE;
  }
  int pos = (int)(operation_sep_slice - mode);

  // если после знака "-"/"+" ничего нет, то аргумент неправильный
  if (pos >= len - 1)
  {
    fprintf(stderr, "Неправильный формат модификации прав "
                    "(пусто после +/-).\n"
                    "Используйте вызов в формате"
                    " '%s'\n",
            call_format);
    return EXIT_FAILURE;
  }

  // Выделим часть слева от +/- для уровня прав
  char id[pos];
  memcpy(id, &mode[0], pos);
  id[pos] = '\0';

  // Собираем виды устанавливаемых прав побитово на основе уже определенных
  // и модифицируем текущие права доступа
  int filemode = file_stat.st_mode;
  for (int i = pos + 1; i < len; i += 1)
  {
    switch (mode[i])
    {
      // Добавляем/убираем права на чтение
      case 'r':
        if (strstr(id, "a"))
        {
          filemode = is_removing ? (filemode & ~S_IROTH) : (filemode | S_IROTH);
        }
        if (strstr(id, "g"))
        {
          filemode = is_removing ? (filemode & ~S_IRGRP) : (filemode | S_IRGRP);
        }
        if (strstr(id, "u"))
        {
          filemode = is_removing ? (filemode & ~S_IRUSR) : (filemode | S_IRUSR);
        }
        break;

      // Добавляем/убираем права на запись
      case 'w':
        if (strstr(id, "a"))
        {
          filemode = is_removing ? (filemode & ~S_IWOTH) : (filemode | S_IWOTH);
        }
        if (strstr(id, "g"))
        {
          filemode = is_removing ? (filemode & ~S_IWGRP) : (filemode | S_IWGRP);
        }
        if (strstr(id, "u"))
        {
          filemode = is_removing ? (filemode & ~S_IWUSR) : (filemode | S_IWUSR);
        }
        break;

      // Добавляем/убираем права на выполнение
      case 'x':
        if (strstr(id, "a"))
        {
          filemode = is_removing ? (filemode & ~S_IXOTH) : (filemode | S_IXOTH);
        }
        if (strstr(id, "g"))
        {
          filemode = is_removing ? (filemode & ~S_IXGRP) : (filemode | S_IXGRP);
        }
        if (strstr(id, "u"))
        {
          filemode = is_removing ? (filemode & ~S_IXUSR) : (filemode | S_IXUSR);
        }
        break;

      // неизвестная буква формата
      default:
        fprintf(stderr, "Неправильный формат [rwx] модификации прав.\n"
                        "Используйте вызов в формате"
                        " '%s'\n",
                call_format);
        return EXIT_FAILURE;
    }
  }

  // Проверяем, будут ли изменены права
  if (filemode != file_stat.st_mode)
  {
    // Изменяем права и проверяем ошибку
    if (chmod(file_path, filemode) != 0)
    {
      fprintf(stderr, "Не удалось сменить права\n");
      return EXIT_FAILURE;
    }
    else {
      printf("Изменены права на файл '%s' c %d на %d\n",
            file_path, file_stat.st_mode, filemode);
    }
  }
  else
  {
    printf("Права не изменяются...\n");
  }

  return 0;
}
