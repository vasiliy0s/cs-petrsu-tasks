#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

// Простое приведение размера файла к Б/КиБ/МиБ/ГиБ
// с округлением
char* format_file_size(blkcnt_t size)
{
  char *s = (char *)malloc(13 * sizeof(char));

  if (size < 1024) {
    sprintf(s, "%lld Б", size);
  }
  else if (size < 1024 * 1024) {
    sprintf(s, "%lld КиБ", size / 1024);
  }
  else if (size < 1024 * 1024 * 1024) {
    sprintf(s, "%lld МиБ", size / 1024 / 1024);
  }
  else {
    sprintf(s, "%lld ГиБ", size / 1024 / 1024 / 1024);
  }

  return s;
}

int print_file_size(char const* filepath)
{
  struct stat link_stat;
  int result;

  // Получаем информацию о файле
  result = lstat(filepath, &link_stat);
  if (result == -1)
  {
    fprintf(stderr, "Запрошенный файл '%s' не найден\n", filepath);
    return -1;
  }

  // Пропускаем не-файлы
  if (!S_ISREG(link_stat.st_mode))
  {
    return 0;
  }

  // Считаем кол-во занимаемой памяти на диске (если блок в ФС == 512 байт)
  blkcnt_t blocks_size = link_stat.st_blocks * 512;
  printf("| %s \t\t| %s \t\t| %s\n",
         format_file_size(blocks_size),
         format_file_size(link_stat.st_size),
         filepath);

  return 0;
}

int main(int argc, char const *argv[])
{
  if (argc < 2) {
    fprintf(stderr, "Не передано ни одного файла для вывода\n");
    return EXIT_FAILURE;
  }

  int result;
  for (int i = 1; i < argc; i += 1) {
    if (i == 1) {
      // Выводим заголовок таблицы
      printf("| занимает по факту \t| размер файла \t| название\n");
    }

    // Выводим информацию о конкретном файле
    result = print_file_size(argv[i]);

    if (result != 0) {
      return EXIT_FAILURE;
    }
  }

  return 0;
}
