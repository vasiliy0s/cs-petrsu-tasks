#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  // Проверяем кол-во аргументов вызова программы
  if (argc != 2)
  {
    fprintf(stderr, "Неправильный вызов программы\n");
    return EXIT_FAILURE;
  }

  // Проверяем наличие символьного файла перед попыткой чтения его значения
  struct stat link_stat;
  if (lstat(argv[1], &link_stat) == -1)
  {
    fprintf(stderr, "Запрошенный файл '%s' не найден\n", argv[1]);
    return EXIT_FAILURE;
  }

  if (!S_ISLNK(link_stat.st_mode))
  {
    fprintf(stderr, "Запрошенный файл '%s' не является ссылкой\n", argv[1]);
    return EXIT_FAILURE;
  }

  int max_size = link_stat.st_size + 1;

  // Так как заранее неизвестно, какой длины будет значение символьной
  // ссылки, выделяем память динамически
  char *linkname;
  linkname = malloc(max_size);
  if (linkname == NULL)
  {
    fprintf(stderr, "Недостаточно памяти для размещения значения ссылки\n");
    return EXIT_FAILURE;
  }

  // Пытаемся прочесть значение, на которое указывает символьный файл
  // -1 говорит о невозможности вписать строку в буффер
  ssize_t r;
  r = readlink(argv[1], linkname, max_size);
  if (r == -1)
  {
    fprintf(stderr, "Невозможно прочесть ссылку '%s'\n", linkname);
    return EXIT_FAILURE;
  }

  // Добавляем нулевой символ в значение ссылки
  linkname[r] = '\0';

  // Выводим значение ссылки
  printf("%s\n", linkname);

  exit(EXIT_SUCCESS);
}
