#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

// Отдельно печатаем права на объект
// filestat - информация о файле, выданная системным вызовом stat()
void print_file_perm(struct stat filestat)
{
  printf("Права на объект: \t");

  switch (filestat.st_mode & S_IFMT)
  {
    case S_IFBLK:
      printf("b");
      break;
    case S_IFCHR:
      printf("c");
      break;
    case S_IFDIR:
      printf("d");
      break;
    case S_IFIFO:
      printf("p");
      break;
    case S_IFLNK:
      printf("l");
      break;
    case S_IFSOCK:
      printf("s");
      break;
    default:
    case S_IFREG:
      printf("-");
      break;
  }

  printf((filestat.st_mode & S_IRUSR) ? "r" : "-");
  printf((filestat.st_mode & S_IWUSR) ? "w" : "-");
  printf((filestat.st_mode & S_IXUSR) ? "x" : "-");
  printf((filestat.st_mode & S_IRGRP) ? "r" : "-");
  printf((filestat.st_mode & S_IWGRP) ? "w" : "-");
  printf((filestat.st_mode & S_IXGRP) ? "x" : "-");
  printf((filestat.st_mode & S_IROTH) ? "r" : "-");
  printf((filestat.st_mode & S_IWOTH) ? "w" : "-");
  printf((filestat.st_mode & S_IXOTH) ? "x" : "-");
  printf("\n");
}

// Форматируем даты (используется несколько раз)
// timestamp - секунды c UNIX-эпохи
char* format_timestamp(long timestamp)
{
  char buffer[26];
  struct tm *tm_info;
  tm_info = localtime(&timestamp);
  strftime(buffer, 26, "%Y:%m:%d %H:%M:%S", tm_info);
  char* str = buffer;
  return str;
}

// Выводим тип файла
// filestat - информация о файле, выданная системным вызовом stat()
void print_file_type_info(struct stat filestat)
{
  char* file_type;
  switch (filestat.st_mode & S_IFMT)
  {
    case S_IFBLK:
      file_type = "блочное устройство";
      break;
    case S_IFCHR:
      file_type = "символьное устройство";
      break;
    case S_IFDIR:
      file_type = "каталог";
      break;
    case S_IFIFO:
      file_type = "конвейер";
      break;
    case S_IFLNK:
      file_type = "символьная ссылка";
      break;
    case S_IFSOCK:
      file_type = "сокет";
      break;
    case S_IFREG:
      file_type = "файл";
      break;
    default:
      file_type = "неизвестно";
      break;
  }

  printf("Тип объекта: \t\t%s\n", file_type);
}

// Выводим всю информацию о файле
// filename - путь к файлу
// filestat - информация о файле, выданная системным вызовом stat()
int print_stat(const char* filename, struct stat filestat)
{
  printf("Путь: \t\t\t%s\n", filename);
  print_file_type_info(filestat);
  if (S_ISBLK(filestat.st_mode) || S_ISCHR(filestat.st_mode))
  {
    printf("Предп. разм. блока: \t%ld байт\n", (long)filestat.st_blksize);
  }
  if (S_ISREG(filestat.st_mode) || S_ISDIR(filestat.st_mode))
  {
    printf("Размер: \t\t%lld байт\n", filestat.st_size);
  }
  printf("Кол-во ссылок: \t\t%d\n", filestat.st_nlink);
  printf("Inode: \t\t\t%lld\n", filestat.st_ino);
  printf("ID владельца: \t\t%d\n", filestat.st_uid);
  printf("ID группы: \t\t%d\n", filestat.st_gid);

  print_file_perm(filestat);
  printf("Прочитан: \t\t%s\n", format_timestamp(filestat.st_atime));
  printf("Изменение прав доступа: %s\n", format_timestamp(filestat.st_ctime));
  printf("Изменен: \t\t%s\n", format_timestamp(filestat.st_mtime));
  return 0;
}

// Основное тело программы, вызываемое с передаваемыми агрументами
int main(int argc, char const *argv[])
{
  if (argc < 2)
  {
    fprintf(stderr, "Неправильный вызов программы\n");
    return EXIT_FAILURE;
  }

  const char *pathname = argv[1];

  if (sizeof(pathname) < 1)
  {
    fprintf(stderr, "Путь '%s' слишком короткий или не указан\n",
            pathname);
    return EXIT_FAILURE;
  }

  struct stat path_stat;

  int result = lstat(pathname, &path_stat);
  if (result != 0)
  {
    fprintf(stderr, "Файл '%s', похоже, не существует. Код ошибки: %d\n",
            pathname, result);
    return EXIT_FAILURE;
  }

  return print_stat(pathname, path_stat);
}
