#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>

/*
 * Скопировано из документации <ftw.h>:
 *
 * Valid flags for the 3rd argument to the function that is passed as the
 * second argument to ftw(3) and nftw(3).  Say it three times fast!
 */
#define FTW_F 0   /* File.  */
#define FTW_D 1   /* Directory.  */
#define FTW_DNR 2 /* Directory without read permission.  */
#define FTW_DP 3  /* Directory with subdirectories visited.  */
#define FTW_NS 4  /* Unknown type; stat() failed.  */
#define FTW_SL 5  /* Symbolic link.  */
#define FTW_SLN 6 /* Sym link that names a nonexistent file.  */

// Предположим, что максимум файлов в папке для ext2 это 10 в 8й
int MAX_FILES = 10 ^ 18;

int ftw(const char *directory,
        int (*funcptr)(const char *file, struct stat *sb, int flag),
        int depth)
{
  // Конец рекурсии
  if (depth < 0)
  {
    return 0;
  }

  int ret;
  int flag;

  // Выделяем память под список читаемый файлов
  char **files_to_read;
  int files_to_read_count = 0;
  files_to_read = malloc(MAX_FILES * sizeof(char *));

  // Получаем информацию о файле (сквозь символическую ссылку)
  struct stat lst;
  int result = lstat(directory, &lst);

  // Невозможно считать файл
  if (result != 0)
  {
    flag = FTW_NS;
  }
  // Обработка файлов
  else if (S_ISREG(lst.st_mode))
  {
    flag = FTW_F;
  }
  // Обработка ссылок
  else if (S_ISLNK(lst.st_mode))
  {
    // Так как заранее неизвестно, какой длины будет значение символьной
    // ссылки, выделяем память динамически
    char *linkname;
    if ((linkname = malloc(FILENAME_MAX)) == NULL)
    {
      fprintf(stderr, "Недостаточно памяти для размещения значения ссылки\n");
      return EXIT_FAILURE;
    }

    // Извлекаем из ФС путь символьной ссылки
    if ((ret = readlink(directory, linkname, FILENAME_MAX)) == -1)
    {
      return ret;
    }


    // Если не получить информацию о ссылке - она,
    // скорее всего, битая
    struct stat symlink_lst;
    int result;
    if ((result = stat(directory, &symlink_lst)) != 0)
    {
      flag = FTW_SLN;
    }
    else
    {
      flag = FTW_SL;
    }
  }
  // Обработка папок
  else if (S_ISDIR(lst.st_mode))
  {
    DIR *dir;
    // Открываем каталог на чтение файлов
    if ((dir = opendir(directory)) != NULL)
    {
      int visited_subdirs = 0;
      struct stat subdir_stat;

      // Читаем файлы друг за другом
      struct dirent *ent;
      while ((ent = readdir(dir)) != NULL)
      {
        // Пропускаем текущий каталог '.' и каталог выше '..'
        if (strcmp(ent->d_name, ".") == 0 ||
            strcmp(ent->d_name, "..") == 0)
        {
          continue;
        }

        // Выделяем память под название файла
        char *path = malloc(MAX_FILES * (FILENAME_MAX + 1));

        // Соедяняем имя папки с именем файла для получения пути
        sprintf(path, "%s/%s", directory, ent->d_name);

        // Проверяем, будет ли перечисляемый файл папкой, для
        // флага FTW_DP, при условии дальнейшей рекурсии
        if (depth > 0 && stat(path, &subdir_stat) == 0)
        {
          if (S_ISDIR(subdir_stat.st_mode))
          {
            visited_subdirs += 1;
          }
        }

          // Сохраняем файл в массиве файлов для чтения
          files_to_read[files_to_read_count++] = path;
      }

      // Закрываем папку
      closedir(dir);

      // Если среди посещенных файлов была хотя бы одна папка,
      // выставляем флаг FTW_DP.
      flag = visited_subdirs > 0 ? FTW_DP : FTW_D;
    }
    // Не удалось считать каталог по причине отсутствия прав доступа
    else
    {
      flag = FTW_DNR;
    }
  }
  else
  {
    return 0;
  }

  // Сначала выполяем callback-функцию над текущим файлом
  if ((ret = funcptr(directory, &lst, flag)) != 0)
  {
    return ret;
  }

  // ...Затем выполняем ftw() рекурсивно по всем вложенным файлам
  for (int i = 0; i < files_to_read_count; i++)
  {
    // Отнимаем от глубины -1 для выполнения условия достижения
    // глубины рекурсии
    if ((ret = ftw(files_to_read[i], funcptr, depth - 1)) != 0)
    {
      return ret;
    }
  }

  return 0;
}

// Просто выводим тип полученного файла, флаг и данный путь
int print_file(const char *file, struct stat *sb, int flag)
{
  // Определяем тип файла для красивого вывода
  char *file_type;
  switch (flag)
  {
    case FTW_F:
      file_type = "файл:\t\t\t";
      break;
    case FTW_D:
      file_type = "директория:\t\t\t";
      break;
    case FTW_DNR:
      file_type = "нечитаемая директория:\t";
      break;
    case FTW_DP:
      file_type = "посещенная директория:\t";
      break;
    case FTW_NS:
      file_type = "неизвестный тип файла:\t";
      break;
    case FTW_SL:
      file_type = "символическая ссылка:\t";
      break;
    case FTW_SLN:
      file_type = "битая символическая ссылка:\t";
      break;
    default:
      file_type = "неизвестный флаг:\t";
      break;
  }

  printf("[%d] %s %s\n", flag, file_type, file);

  return 0;
}

int main(int argc, char const *argv[])
{
  if (argc != 3)
  {
    fprintf(stderr, "Неправильный вызов программы\n"
                    "Используйте './ftw [dirname] [depth]'");
    return EXIT_FAILURE;
  }

  const char* directory_name = argv[1];
  const int depth = atoi(argv[2]);

  return ftw(directory_name, print_file, depth);
}
