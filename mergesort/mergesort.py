from sys import argv


def merge(arr, debug=False):
  arr_len = len(arr)

  if (arr_len <= 1):
    return arr

  middle = int(arr_len / 2)
  left_arr = merge(arr[:middle], debug=debug)
  right_arr = merge(arr[middle:], debug=debug)

  sorted_arr = [None] * arr_len
  left_index = 0
  right_index = 0
  left_max_index = len(left_arr) - 1
  right_max_index = len(right_arr) - 1
  sorted_index = 0
  while sorted_index < arr_len:
    left_val = left_arr[left_index] if left_index <= left_max_index else None
    right_val = right_arr[right_index] if right_index <= right_max_index else None
    if not left_val:
      sorted_arr[sorted_index] = right_val
      right_index += 1
    elif not right_val or left_val <= right_val:
      sorted_arr[sorted_index] = left_val
      left_index += 1
    else:
      sorted_arr[sorted_index] = right_val
      right_index += 1
    sorted_index += 1

  if debug:
    print("middle: %s, arr: %s, left_arr: %s, right_arr: %s, -> sorted_arr: %s" %
          (middle, arr, left_arr, right_arr, sorted_arr))

  return sorted_arr

def main():
  numbers = []
  for num in argv[1:]:
    try:
      num = int(float(num))
      numbers.append(num)
    except:
      continue

  debug=False
  merged = merge(numbers, debug=debug)
  print("mergesort(): %s -> %s" % (numbers, merged))

if __name__ == '__main__':
  main()
