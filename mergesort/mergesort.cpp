#include <iostream>
#include <fstream>
using namespace std;

// Рекурсивная сортировка выбранного отрезка массива
int* mergesort(int* numbers, int start, int end)
{
  // Определяем длина отрезка
  int len = end - start + 1;
  int *sorted = new int[len];

  if (len <= 1) {
    // Нечего сортировать, если у нас только один элемент
    // (конец рекурсии)
    sorted[0] = numbers[start];
  } else {
    // Определяем индекс дальнейшего разбиения отрезка
    int median = start + (end - start) / 2;

    // Рекурсивно сортируем правую и левую части
    int *left_arr = mergesort(numbers, start, median);
    int *right_arr = mergesort(numbers, median + 1, end);

    // Определяем индексы для чтения из отсортированных под-отрезков
    int left_max_index = median - start, right_max_index = end - (median + 1);
    int left_index = 0, right_index = 0;

    for (int sorted_index = 0; sorted_index < len; sorted_index++)
    {
      // Если левая часть отрезка закончилась, берем только из
      // уже (рекурсивно) отсортированной правой части
      if (left_index > left_max_index)
      {
        sorted[sorted_index] = right_arr[right_index++];
      // Если правая часть отрезка закончилась, берем только из
      // уже (рекурсивно) отсортированной левой части
      } else if (right_index > right_max_index)
      {
        sorted[sorted_index] = left_arr[left_index++];
      // Если следующий элемент из левого отрезка меньше (или равен) элемента
      // из правого,то ставим его "раньше" правого - сама сортировка
      } else if (left_arr[left_index] <= right_arr[right_index])
      {
        sorted[sorted_index] = left_arr[left_index++];
      // И, наоборот, ставим следующий элемент из правого уже отсортированного
      // отрезка "раньше" левого, если правый меньше левого
      } else
      {
        sorted[sorted_index] = right_arr[right_index++];
      }
    }
  }

  // Возвращаем отсортированный отрезок [start, end] как новый массив
  return sorted;
}

int main()
{
  int len;

  freopen("input.txt", "r", stdin);
  // Читаем длину массива из файла
  cin >> len;
  // Читаем сам массив из файла, пока числа не закончатся,
  // или пока не наступит конец длины массива
  int* numbers = new int[len];
  int num, i = 0;
  while (cin >> num && i < len)
  {
    numbers[i++] = num;
  }
  fclose(stdin);

  // Выполняем сортировку
  int* sorted = mergesort(numbers, 0, len-1);

  freopen("output.txt", "w", stdout);
  // Записываем результат обратно в файл
  for (int i = 0; i < len; i++)
  {
    cout << sorted[i] << " ";
  }
  cout << endl;
  fclose(stdout);

  return 0;
}
