/**
 * print_file_wchars.c -- утилита форматированного вывода
 * содержимого файла с поддержкой UNICODE
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <wchar.h>
#include <locale.h>

#define USAGE "Использование: ./print_file_wchars <file_path>"

int print_file_wchars(const char *file_path)
{
  FILE *file;
  wint_t wc;
  int print_result;

  // Пытаемся открыть файл для чтения
  if ((file = fopen(file_path, "r")) == NULL)
  {
    fprintf(stderr, "Не получается открыть файл '%s' для чтения\n", file_path);
    return EXIT_FAILURE;
  }

  setlocale(LC_ALL, "");

  // Считываем char из файла по текущему указателю
  while ((wc = fgetwc(file)) != WEOF)
  {
    // Выводим символ с пробелом
    // wc==10 - символ перевода строки, который сбивает форматирование
    // и следующая строчка получается со сдвигом слева
    print_result = fprintf(stdout, wc != 10 ? "%lc " : "%lc", wc);

    // Проверяем ошибку вывода
    if (print_result < 0)
    {
      fprintf(stderr, "Не получается вывести содержимое файла '%s' в stdout\n", file_path);
      return EXIT_FAILURE;
    }
  }

  // Добавляем EOF как новая строчка для удобства чтения вывода
  printf("\n");

  return 0;
}

int main(int argc, char const *argv[])
{
  const char *file_path;
  struct stat file_stat;

  // Проверка аргументов
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  file_path = argv[1];

  // Проверяем наличие файла
  if (stat(file_path, &file_stat) != 0)
  {
    fprintf(stderr, "Невозможно прочесть файл '%s'\n", file_path);
    return EXIT_FAILURE;
  }

  return print_file_wchars(file_path);
}
