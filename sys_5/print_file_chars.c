/**
 * print_file_chars.c -- утилита форматированного вывода
 * содержимого файла
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>

#define USAGE "Использование: ./print_file_chars <file_path>"

int print_file_chars(const char *file_path)
{
  FILE *file;
  int c;
  int print_result;

  // Пытаемся открыть файл для чтения
  if ((file = fopen(file_path, "r")) == NULL)
  {
    fprintf(stderr, "Не получается открыть файл '%s' для чтения\n", file_path);
    return EXIT_FAILURE;
  }

  // char buffer [2];

  // Считываем char из файла по текущему указателю
  while ((c = fgetc(file)) != EOF)
  {
    // Выводим символ с пробелом, если это реально
    // c==10 - символ перевода строки, который сбивает форматирование
    // и следующая строчка получается со сдвигом слева
    print_result = fprintf(stdout, c != 10 ? "%c " : "%lc", c);

    // Проверяем ошибку вывода
    if (print_result < 0)
    {
      fprintf(stderr, "Не получается вывести содержимое файла '%s' в stdout\n", file_path);
      return EXIT_FAILURE;
    }
  }

  // Добавляем EOF как новая строчка для удобства чтения вывода
  printf("\n");

  return 0;
}

int main(int argc, char const *argv[])
{
  const char *file_path;
  struct stat file_stat;

  // Проверка аргументов
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  file_path = argv[1];

  // Проверяем наличие файла
  if (stat(file_path, &file_stat) != 0)
  {
    fprintf(stderr, "Невозможно прочесть файл '%s'\n", file_path);
    return EXIT_FAILURE;
  }

  return print_file_chars(file_path);
}
