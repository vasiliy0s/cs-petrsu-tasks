/**
 * cp.c -- утилита копирования файла с разными режимами
 * буферизации
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <sys/stat.h>

// Доступные размеры буффера
#define BUF_SIZE_BYTE 1
#define BUF_SIZE_KILO_BYTE 1024
#define BUF_SIZE_MEGA_BYTE (1024 * 1024)

// Пример использования программы храним в ОДНОМ месте
#define USAGE "Использование: ./cp <file_path_from> <file_path_to> [-b|-k|-m|-s] [-n]"

// Функция копирования файла из пути file_path_from в путь file_path_to
int copy_file(const char *file_path_from, const char *file_path_to, long buf_size, int no_buffer_option)
{
  FILE *file_from, *file_to;
  char copying_buffer[buf_size];
  size_t bytes;

  // Пытаемся открыть файл для чтения содержимого ("r" указанно в режиме)
  // "b" в режиме означает побитовое чтение
  if ((file_from = fopen(file_path_from, "rb")) == NULL)
  {
    fprintf(stderr, "Невозможно прочесть файл '%s'\n", file_path_from);
    return EXIT_FAILURE;
  }

  // Открываем файл на запись через fopen.
  // Режим "wb+" - перезаписать файл через установку fseek
  // в начало файла или создать и открыть, если не существует
  // "b" в режиме означает побитовую запись
  if ((file_to = fopen(file_path_to, "wb+")) == NULL)
  {
    fprintf(stderr, "Не получается открыть файл '%s' для записи (или создать новый)\n", file_path_to);
    fprintf(stderr, "Проверьте права доступа до файла и попробуйте снова\n");
    return EXIT_FAILURE;
  }

  // Выключаем буфферы чтения/записи
  if (no_buffer_option > 0)
  {
    setbuf(file_from, NULL);
    setbuf(file_to, NULL);
  }

  // Копируем содержимое файла поблочно по размеру блока buf_size,
  // пока не закончится файл
  while (0 < (bytes = fread(copying_buffer, 1, buf_size, file_from)))
  {
    // Записываем порцию содержимого файла из буффера размера bytes
    if (fwrite(copying_buffer, 1, bytes, file_to) != bytes)
    {
      fprintf(stderr, "Не получается записать порцию данных в файл '%s'\n", file_path_to);
      return EXIT_FAILURE;
    }
  }

  // Закрываем дескрипторы файлов
  if (fclose(file_from) != 0)
  {
    fprintf(stderr, "Не получается закрыть файл '%s'\n", file_path_from);
    return EXIT_FAILURE;
  }

  if (fclose(file_to) != 0)
  {
    fprintf(stderr, "Не получается закрыть файл '%s'\n", file_path_to);
    return EXIT_FAILURE;
  }

  return 0;
}

int main(int argc, char const *argv[])
{
  int result;
  long buf_size;
  struct timeval tv_start, tv_end;
  const char *file_path_from, *file_path_to, *buf_size_option, *no_buf_option;
  struct stat file_stat_from;
  int no_buffer_option = 0;

  // Проверка аргументов
  if (argc < 3 || argc > 5)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  file_path_from = argv[1];
  file_path_to = argv[2];
  buf_size_option = argv[3];
  no_buf_option = argv[4];
  buf_size = BUF_SIZE_MEGA_BYTE;

  // Считаываем информацию о файле
  if (stat(file_path_from, &file_stat_from) != 0)
  {
    fprintf(stderr, "Невозможно прочесть файл '%s'\n", file_path_from);
    return EXIT_FAILURE;
  }

  // Проверяем опцию размера буффера
  // при запуске с ключом -b
  if (buf_size_option == NULL)
  {
    buf_size = BUF_SIZE_MEGA_BYTE;
  }
  else if (strncmp(buf_size_option, "-b", 2) == 0)
  {
    buf_size = BUF_SIZE_BYTE;
  }
  // при запуске с ключом -k
  else if (strncmp(buf_size_option, "-k", 2) == 0)
  {
    buf_size = BUF_SIZE_KILO_BYTE;
  }
  // при запуске с ключом -m
  else if (strncmp(buf_size_option, "-m", 2) == 0)
  {
    buf_size = BUF_SIZE_MEGA_BYTE;
  }
  // при запуске с ключом -s
  else if (strncmp(buf_size_option, "-s", 2) == 0)
  {
    buf_size = (long)file_stat_from.st_blksize;
  }
  else
  {
    fprintf(stderr, "Неверно указана опция '%s' [-b|-k|-m|-s]\n", buf_size_option);
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  // Проверяем опцию отключения буффера
  if (no_buf_option != NULL)
  {
    if (strncmp(no_buf_option, "-n", 2) == 0)
    {
      no_buffer_option = 1;
    }
    else
    {
      fprintf(stderr, "Неверно указана опция [-n] '%s' \n", no_buf_option);
      fprintf(stderr, "%s\n", USAGE);
      return EXIT_FAILURE;
    }
  }

  printf("Размер файла = %ld байт\n", (long)file_stat_from.st_size);
  printf("Размер буфера = %ld байт\n", buf_size);
  if (no_buffer_option > 0)
  {
    printf("Буфер выключен\n");
  }
  else {
    printf("Буфер включен\n");
  }

  // Копируем файл и замеряем время работы
  gettimeofday(&tv_start, NULL); // точное время старта копирования
  result = copy_file(file_path_from, file_path_to, buf_size, no_buffer_option);
  gettimeofday(&tv_end, NULL); // точное время завершения

  // Выводим время работы только если copy_file() отработала без ошибок
  if (result == 0)
  {
    // Считаем разницу между временем старта и временем окончания
    // как время испольнения программы, в секундах (tv_sec),
    // с учетом миллисекунд (tv_usec)
    double exec_time = (double)(tv_end.tv_usec - tv_start.tv_usec) / 1000000 +
                       (double)(tv_end.tv_sec - tv_start.tv_sec);
    printf("Время копирования = %f сек.\n", exec_time);
  }

  return result;
}
