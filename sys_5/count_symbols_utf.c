/**
 * count_symbols_utf.c -- утилита подсчета кол-ва
 * символов в переданной как аргумент строке
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <wchar.h>
#include <locale.h>

#define USAGE "Использование: ./count_symbols_utf 'utf-8 строка'"

// Простая проверка символов в Unicode строке
int count_symbols_utf(const char *symbols)
{
  int i = 0, j = 0;

  while (symbols[i])
  {
    // Находим бит разделения символов
    // и засчитываем как отдельный символ
    // в кодировке UTF-8
    if ((symbols[i] & 0xC0) != 0x80)
    {
      j++;
    }

    i++;
  }

  return j;
}

int main(int argc, char const *argv[])
{
  int count;

  // Проверка аргументов
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  count = count_symbols_utf(argv[1]);

  printf("Строка содержит '%d' символов\n", count);

  return 0;
}
