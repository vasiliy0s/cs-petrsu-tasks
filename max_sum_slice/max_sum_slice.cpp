#include <iostream>
#include <fstream>
using namespace std;

int max_sum_slice(int* arr, int len) {
  int max_sum = 0, cur_sum = 0;
  for (int i = 0; i < len; i++) {
    cur_sum = max(arr[i], cur_sum + arr[i]);
    max_sum = max(max_sum, cur_sum);
    // cout << "arr[" << i << "]=" << arr[i]
    //      << ", cur_sum=" << cur_sum
    //      << ", max_sum=" << max_sum
    //      << endl;
  }
  return max_sum;
}

int main()
{
  int len;

  freopen("input.txt", "r", stdin);
  cin >> len;
  int *numbers = new int[len];
  int num, i = 0;
  while (cin >> num && i < len)
  {
    numbers[i++] = num;
  }
  fclose(stdin);

  int max_sum = max_sum_slice(numbers, len);

  freopen("output.txt", "w", stdout);
  cout << max_sum << endl;
  fclose(stdout);

  return 0;
}
