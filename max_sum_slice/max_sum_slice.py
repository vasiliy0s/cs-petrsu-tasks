from sys import argv

def max_sum_slice(arr, debug=False): # arr = [1,2,,-4,556,...]
  best = 0
  cur_sum = 0
  for n in arr:
    cur_sum = max(n, cur_sum + n)
    best = max(cur_sum, best)
  return best

def main():
  numbers = []
  for num in argv[1:]:
    try:
      num = int(float(num))
      numbers.append(num)
    except:
      continue

  debug=False
  result = max_sum_slice(numbers, debug=debug)
  print("max_sum_slice(): %s -> %s" % (numbers, result))

if __name__ == '__main__':
  main()
