/**
 * byte_order.c -- утилита для отображения введенного
 * 32-х разрядного числа в порядках, поддерживаемом
 * текущей архитектурой, и жестко заданным сетевым
 * порядком.
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>

#define USAGE "Использование: ./byte_order 0x01020304"

void
print_bytes(const char * order_type, uint32_t num);

void
print_value(uint32_t num);

int
main(int argc, char const *argv[])
{
  uint32_t input_num;
  char * end;

  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы!\n%s\n", USAGE);
    exit(EXIT_FAILURE);
  }

  input_num = strtol(argv[1], &end, 16);
  if (input_num == 0L)
  {
    fprintf(stderr, "Введено неверное число. Невозможно конвертировать!\n");
    exit(EXIT_FAILURE);
  }

  print_bytes("host", input_num);
  print_bytes("net", htonl(input_num));
  // print_bytes("dev", ntohl(htonl(input_num)));
  print_value(htonl(input_num));

  return 0;
}

void
print_bytes(const char * order_type, uint32_t num)
{
  printf("%s byte order:\n", order_type);
  printf("s_addr=0x%08x, "
         "0x%02x:0x%02x:0x%02x:0x%02x\n",
         num,
         ((char *)(&num))[0],
         ((char *)(&num))[1],
         ((char *)(&num))[2],
         ((char *)(&num))[3]);
}

void
print_value(uint32_t num)
{
  printf("s=%x.%x.%x.%x\n",
        ((char *)(&num))[0],
        ((char *)(&num))[1],
        ((char *)(&num))[2],
        ((char *)(&num))[3]);
}
