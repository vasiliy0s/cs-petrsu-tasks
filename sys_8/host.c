/**
 * host.c -- утилита для отображения IP-адреса
 * для введенного хоста
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#define USAGE "Использование: ./host vk.com"

int lookup(const char * host);

int
main(int argc, char const *argv[])
{
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы!\n%s\n", USAGE);
    exit(EXIT_FAILURE);
  }


  return lookup(argv[1]);
}

int lookup(const char *host)
{
  // Ставим максимальную длину распознования как в IPv6,
  // так как она длиннее IPv4
  int maxlen = INET6_ADDRSTRLEN;

  struct addrinfo addr_hints, *addr_info;
  char addr_strv6[maxlen];
  void *ptr;

  // Выделяем память под addr_hints
  memset(&addr_hints, 0, sizeof(addr_hints));

  // Выставляем флаги (опции) для getaddrinfo()
  addr_hints.ai_family = PF_UNSPEC; // запрашиваем IPv4 & IPv6 адреса
  addr_hints.ai_socktype = SOCK_STREAM; // юзаем TCP/IP
  addr_hints.ai_flags |= AI_CANONNAME; // запрашиваем канонич. доменное имя

  // Собственно, получаем список адресов в *addr_info с опциями addr_hints
  // по хосту host
  int getaddrinfo_result;
  if ((getaddrinfo_result = getaddrinfo(host, NULL, &addr_hints, &addr_info)) != 0)
  {
    fprintf(stderr, "Невозможно получить информацию о хосте %s "
            "из-за ошибки: \n->\t%s!\n",
            host,
            gai_strerror(getaddrinfo_result));
    exit(EXIT_FAILURE);
  }

  // Считываем все найденные записи для полученной информации о хосте
  while (addr_info)
  {
    // Преобразуем адрес в человекочитаемый вид
    if (inet_ntop(addr_info->ai_family,
                  addr_info->ai_addr->sa_data,
                  addr_strv6,
                  maxlen) == NULL)
    {
      fprintf(stderr, "Невозможно преобразовать информацию о хосте %s!\n",
                      host);
      exit(EXIT_FAILURE);
    }

    // Считываем PTR согласно автоматически определенному протоколу
    // IPv4
    if (addr_info->ai_family == AF_INET)
    {
      ptr = &((struct sockaddr_in *)addr_info->ai_addr)->sin_addr;
    }
    // IPv6
    else if (addr_info->ai_family == AF_INET6)
    {
      ptr = &((struct sockaddr_in6 *)addr_info->ai_addr)->sin6_addr;
    }
    else
    {
      addr_info = addr_info->ai_next;
      continue;
    }

    if (inet_ntop(addr_info->ai_family,
                  ptr,
                  addr_strv6,
                  maxlen) == NULL)
    {
      fprintf(stderr, "Невозможно преобразовать информацию о хосте %s!\n",
              host);
      exit(EXIT_FAILURE);
    }

    // Выводим инфу
    printf("IPv%d address: %s (%s)\n",
           addr_info->ai_family == PF_INET6 ? 6 : 4,
           addr_strv6,
           addr_info->ai_canonname);

    // Переходим к след. записи addrinfo
    addr_info = addr_info->ai_next;
  }

  // Освобождаем память
  freeaddrinfo(addr_info);

  return 0;
}
