/**
 * ip.c -- утилита для получения имени хоста по
 * введенному IP-адресу
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>

#define USAGE "Использование: ./ip 87.240.180.136"

int lookupIPv4(char * ip);

int
main(int argc, char const *argv[])
{
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы!\n%s\n", USAGE);
    return EXIT_FAILURE;
  }

  return lookupIPv4((char * )(argv[1]));
}

int lookupIPv4(char *ip)
{
  // Ковертируем "Internet standard dot notation"
  // в сетевой адрес
  struct in_addr addr;
  if (inet_aton(ip, &addr) == 0)
  {
    fprintf(stderr, "Невозможно конвертировать '%s' в сетевой адрес\n",
                    ip);
    return EXIT_FAILURE;
  }

  // Задаем параметры для определенмя IPv4 в getnameinfo
  struct sockaddr_in *sa_ipv4 = calloc(1, sizeof(*sa_ipv4));
  sa_ipv4->sin_family = AF_INET;
  sa_ipv4->sin_len = sizeof(ip);
  sa_ipv4->sin_addr = addr;

  // Запрашиваем getnameinfo() для записи результата в hbuf
  // * флаг NI_NAMEREQD - ненайденный адрес считаем ошибкой
  struct sockaddr *sa = (struct sockaddr *)sa_ipv4;
  char hbuf[NI_MAXHOST];
  int error;
  error = getnameinfo(sa, sizeof(sa), hbuf, sizeof(hbuf), NULL, 0, NI_NAMEREQD);
  free(sa);

  // Проверяем ошибку и выводим результат
  if (error == 0)
  {
    printf("Для IPv4=%s найден host=%s\n", ip, hbuf);
    return 0;
  }
  else
  {
    char * error_type;
    switch (error) {
      case EAI_AGAIN: error_type = "EAI_AGAIN"; break;
      case EAI_BADFLAGS: error_type = "EAI_BADFLAGS"; break;
      case EAI_FAIL: error_type = "EAI_FAIL"; break;
      case EAI_FAMILY: error_type = "EAI_FAMILY"; break;
      case EAI_MEMORY: error_type = "EAI_MEMORY"; break;
      case EAI_NONAME: error_type = "EAI_NONAME"; break;
      case EAI_SYSTEM: error_type = "EAI_SYSTEM"; break;
      case 0: error_type = "0"; break;
      default: error_type = "Unknown Error"; break;
    }
    fprintf(stderr, "Ошибка определения хоста: %s\n", error_type);
    return EXIT_FAILURE;
  }

}
