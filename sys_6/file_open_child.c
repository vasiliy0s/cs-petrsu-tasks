/**
 * file_open_child.c -- утилита для проверки
 * совместного использования текущей позиции файла
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#define USAGE "Использование: ./file_open_child <path_to_file>"

int fd, parent_position, child_position;

int main(int argc, char const *argv[])
{
  char const *file_path;
  pid_t pid;
  int common_position = 0;

  // Проверяем агрументы вызова
  if (argc != 2)
  {
    fprintf(stderr, "Неверный вызов программы\n%s\n", USAGE);
    return EXIT_FAILURE;
  }

  file_path = argv[1];

  // Пытаемся открыть файл только для чтекния (O_RDONLY)
  if ((fd = open(file_path, O_RDONLY)) == -1)
  {
    fprintf(stderr, "Невозможно открыть файл '%s'\n"
                    "Убедитесь, что файл существует и доступен для чтения", file_path);
  }

  // Форкаем процесс
  if ((pid = fork()) < 0)
  {
    fprintf(stderr, "Невозможно вызвать дочерний процесс\n");
    return EXIT_FAILURE;
  }

  // Проверяем, что мы в дочернем процессе
  else if (pid == 0)
  {
    if ((child_position = lseek(fd, common_position, SEEK_SET)) == -1)
    {
      fprintf(stderr, "Невозможно получить позицию в файле '%s' для дочернего процесса\n", file_path);
      return EXIT_FAILURE;
    }
  }

  // Иначе мы в родительском процессе
  else
  {
    // Ждем, пока дочерняя программа не завершится
    if (waitpid(pid, NULL, 0) != pid)
    {
      fprintf(stderr, "Ошибка waitpid\n");
      return EXIT_FAILURE;
    }

    // Получаем позицию чтения файла в родительском процессе
    if ((parent_position = lseek(fd, common_position, SEEK_SET)) == -1)
    {
      fprintf(stderr, "Невозможно получить позицию в файле '%s' для родительского процесса\n", file_path);
      return EXIT_FAILURE;
    }

    printf("[%s] Позиция в родительском (=%d) и дочернем (=%d) процессах %s\n",
           parent_position == child_position ? "✓" : "✕",
           parent_position,
           child_position,
           parent_position == child_position ? "совпадают" : "НЕ совпадают");
  }

  return 0;
}
