/**
 * fork.c -- утилита для запуска дочерней программы
 * с выводом информации о результате запуска
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/stat.h>

// Пример использования программы храним в ОДНОМ месте
#define USAGE "Использование: ./fork <path_to_child_program> <arg1> [<arg2>, ...]"

void print_exit_status(pid_t subprogram_statloc)
{
  if (WIFEXITED(subprogram_statloc) != 0)
  {
    printf("Нормальное завершение процесса c кодом %d\n", WEXITSTATUS(subprogram_statloc));
  }
  else if (WIFSIGNALED(subprogram_statloc) != 0)
  {
    printf("Аварийное завершение процесса с сигналом %d\n", WTERMSIG(subprogram_statloc));
  }
  else if (WIFSTOPPED(subprogram_statloc) != 0)
  {
    printf("Дочерний процесс остановлен по сигналу %d\n", WSTOPSIG(subprogram_statloc));
  }
  else if (WTERMSIG(subprogram_statloc) != 0)
  {
    printf("Дочерний процесс был остановлен по сигналу %d"
           ", но продолжил работу\n",
           WSTOPSIG(subprogram_statloc));
  }
}

int main(int argc, char const *argv[])
{
  pid_t fork_pid;

  fork_pid = fork();

  if (argc < 2)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  // Если мы не смогли раздвоить текущий процесс...
  if (fork_pid < 0)
  {
    fprintf(stderr, "Невозможно вызвать дочерний процесс\n");
    return EXIT_FAILURE;
  }
  // Если мы запустились как дочерний процесс, который
  // должен запустить другую программу и вывести результат
  // ее завершения
  else if (fork_pid == 0)
  {
    int exec_code, stat_result;
    char const *path_to_program = argv[1];
    struct stat path_to_program_stat;

    if ((stat_result = lstat(path_to_program, &path_to_program_stat)) != 0)
    {
      fprintf(stderr, "Программа '%s', похоже, не существует. Код ошибки: %d\n",
              path_to_program, stat_result);
      return EXIT_FAILURE;
    }

    // Запускаем программу, переданную в кач. первого аргумента,
    // с пробрасыванием 3х следующих аргументов как опции
    // дочерней программы
    exec_code = execl(path_to_program, argv[2], argv[3], argv[4], NULL);
    if (exec_code < 0)
    {
      fprintf(stderr, "Ошибка запуска переданной по аргументу программы c кодом %d\n", errno);
      return EXIT_FAILURE;
    }
  }
  // Находясь в родительском процессе, только
  // проверяем аргументы вызова
  else {
    int subprogram_statloc;
    pid_t subprogram_pid;

    if (argc < 2)
    {
      fprintf(stderr, "Неверный вызов программы\n%s\n", USAGE);
      return EXIT_FAILURE;
    }

    // Ждем, пока дочерняя программа не завершится
    if ((subprogram_pid = waitpid(fork_pid, &subprogram_statloc, 0)) != fork_pid)
    {
      fprintf(stderr, "Ошибка waitpid\n");
      return EXIT_FAILURE;
    }

    print_exit_status(subprogram_statloc);
  }

  return 0;
}
