/**
 * system.c -- утилита для выполнения системной команды
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>

char const *command = "echo 'Зима близко' | sed s/близко/через\\ месяц/";

int main(int argc, char const *argv[])
{
  int code;

  if (argc > 1)
  {
    fprintf(stderr, "Неверный вызов программы");
    return EXIT_FAILURE;
  }

  if ((code = system(command)) < 0)
  {
    fprintf(stderr, "Невозможно выполнить команду \"%s\"", command);
    return EXIT_FAILURE;
  }
  else
  {
    printf("Команда \"%s\" завершилась с кодом %d\n", command, code);
  }
  return 0;
}
