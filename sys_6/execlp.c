/**
 * execlp.c -- утилита - аналог функции execlp
 *
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>

char const *USAGE = "Использование: ./execlp <command> [arg1, [arg2, [argv3]]]";

// Выполнение команды с макс. 3мя аргументами через execl
int exec_command(char const *command,
                 int argc,
                 char const *arg1,
                 char const *arg2,
                 char const *arg3)
{
  int exec_code;

  printf("Пытаемся выполнить программу '%s' c кол-вом аргументов = %d...\n",
          command, argc);

  // Выполняем программу с прокинутыми агрументами (l-суффикс для exec)
  switch (argc)
  {
    case 0:
      exec_code = execl(command, NULL);
      break;
    case 1:
      exec_code = execl(command, arg1, NULL);
      break;
    case 2:
      exec_code = execl(command, arg1, arg2, NULL);
      break;
    case 3:
      exec_code = execl(command, arg1, arg2, arg3, NULL);
      break;
    default:
      fprintf(stderr, "Неподдерживаемое число аргументов =%d для вызова команды '%s'\n", argc, command);
      return EXIT_FAILURE;
  }

  // Не удалось выполнить команду
  if (exec_code < 0)
  {
    fprintf(stderr, "Ошибка выполнения команды '%s' c кодом %d\n", command, errno);
    return EXIT_FAILURE;
  }
  else
  {
    printf("Команда выполнена и завершилась с кодом=%d\n", exec_code);
  }

  return 0;
}

int main(int argc, char const *argv[])
{
  const char *command_bin;

  // Проверяем агрументы вызова
  if (argc < 2)
  {
    fprintf(stderr, "Неверный вызов программы\n%s\n", USAGE);
    return EXIT_FAILURE;
  }

  command_bin = argv[1];
  printf("command_bin=%s\n", command_bin);

  // Проверка пути команды на наличие абсолютного/относительного пути
  // Если так, то искать command_bin в PATH не требуется
  if (strncmp(command_bin, "/", 1) == 0 ||
      strncmp(command_bin, "./", 2) == 0 ||
      strncmp(command_bin, "../", 3) == 0)
  {
    return exec_command(command_bin, argc - 2, argv[2], argv[3], argv[4]);
  }

  // ...иначе требуется найти исполняемый файл среди путей переменной PATH
  else {
    char *PATH = getenv("PATH");
    printf("Переменная окружения PATH=%s\n", PATH);

    struct stat path_stat;
    char *path;
    // Обходим каждый сегмент PATH по разделителю ":"
    while ((path = strsep(&PATH, ":")))
    {
      // printf("path=%s\n", path);

      // Получаем полный путь до исполняемого файла
      char command_bin_path[2048];
      strcpy(command_bin_path, path);
      strcat(command_bin_path, "/");
      strcat(command_bin_path, command_bin);

      // Проверяем наличие и доступность файла
      if (lstat(command_bin_path, &path_stat) == 0)
      {
        return exec_command(command_bin_path, argc - 2, argv[2], argv[3], argv[4]);
      }
    }

    // Обрабатываем случай, что команда не найдена
    // (иначе внутри while отработал бы return!)
    fprintf(stderr, "Команда '%s' не найдена!\n", command_bin);
    return EXIT_FAILURE;
  }

  return 0;
}
