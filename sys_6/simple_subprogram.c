#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
  printf("========\n");
  printf("\tВызвана программа 'simple_subprogram'");

  if (argc >= 1)
  {
    printf(" со следующими агрументами:\n");

    for (int i = 1; i <= argc; i++)
    {
      printf("\t\t%d) %s\n", i, argv[i-1]);
    }
  }
  else
  {
    printf(" без агрументов!\n");
  }
  printf("========\n");

  return 0;
}
