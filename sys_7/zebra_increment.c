/**
 * sigprocmask.c -- утилита для поочередного увеличения
 * числа в одном файле с использованием сигналов в
 * качестве гарантии правильного выполнения записи
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
// #include <unistd.h>
// #include <errno.h>
#include <sys/wait.h>
#include <signal.h>
// #include <string.h>

#include "./includes/apue.h"

static volatile sig_atomic_t sigflag; /* set nonzero by sig handler */
static sigset_t newmask, oldmask, zeromask;

static void
sig_usr(int signo) /* one signal handler for SIGUSR1 and SIGUSR2 */
{
  sigflag = 1;
}

void TELL_WAIT(void)
{
  if (signal(SIGUSR1, sig_usr) == SIG_ERR)
    err_sys("signal(SIGUSR1) error");
  if (signal(SIGUSR2, sig_usr) == SIG_ERR)
    err_sys("signal(SIGUSR2) error");
  sigemptyset(&zeromask);
  sigemptyset(&newmask);
  sigaddset(&newmask, SIGUSR1);
  sigaddset(&newmask, SIGUSR2);

  /*
	 * Block SIGUSR1 and SIGUSR2, and save current signal mask.
	 */
  if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
    err_sys("SIG_BLOCK error");
}

void TELL_PARENT(pid_t pid)
{
  kill(pid, SIGUSR2); /* tell parent we're done */
}

void WAIT_PARENT(void)
{
  while (sigflag == 0)
    sigsuspend(&zeromask); /* and wait for parent */
  sigflag = 0;

  /*
	 * Reset signal mask to original value.
	 */
  if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0)
    err_sys("SIG_SETMASK error");
}

void TELL_CHILD(pid_t pid)
{
  kill(pid, SIGUSR1); /* tell child we're done */
}

void WAIT_CHILD(void)
{
  while (sigflag == 0)
    sigsuspend(&zeromask); /* and wait for child */
  sigflag = 0;

  /*
	 * Reset signal mask to original value.
	 */
  if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0)
    err_sys("SIG_SETMASK error");
}

// Функция инкрементирования числа в файле
int increment_file_value(const char * message_prefix)
{
  FILE * f;
  int number, scan_result;
  const char * filename = "increment.txt";

  if ((f = fopen(filename, "rw")) < 0)
  {
    fprintf(stderr, "Невозможно открыть файл %s", filename);
    return EXIT_FAILURE;
  }

  scan_result = fscanf(f, "%d", &number);

  // пустой файл или его содержимое невозможно преобразовать
  // в число, выставляем number == 0
  if (scan_result == EOF || scan_result == 0)
  {
    number = 0;
  }

  number += 1;

  if (fprintf(f, "%d", number) <= 0)
  {
    fprintf(stderr, "Невозможно записать числов в файл %s", filename);
    return EXIT_FAILURE;
  }

  printf("%s записал в файл %s число %d", message_prefix, filename, number);

  return 0;
}

int main(int argc, char const *argv[])
{


  return 0;
}
