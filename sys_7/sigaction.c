/**
 * sigaction.c -- утилита для установки хендлера
 * сигнала на дочерний процесс
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

// Пример использования программы храним в ОДНОМ месте
#define USAGE "Использование: ./sigaction"

// Обработчик сигнала с условием задачи
void sig_hander(int sig, siginfo_t * sig_info, void *ucontext)
{
  pid_t children_pid;
  int status;

  if (sig != SIGCHLD)
  {
    // Ошибка обработки - получен неверный сигнал
    exit(EXIT_FAILURE);
  }

  printf("Получен сигнал SIGCHLD=%d в процессе pid=%d "
        "со следующими данными:\n", sig, getpid());

  // Выводим информацию о сигнале
  printf("\tsig_info.si_pid:\t%d\n", sig_info->si_pid); // PID
  printf("\tsig_info.si_uid:\t%d\n", sig_info->si_uid); // UID владельца
  printf("\tsig_info.si_status:\t%d\n", sig_info->si_status); // статус выхода

  // Ждем, пока дочерняя программа не завершится
  // и выводим возможные результаты waitpid
  if ((children_pid = waitpid(sig_info->si_pid, &status, 0)) < -1)
  {
    printf("waitpid: Нужно ждать любого дочернего процесса, "
           "идентификатор группы процессов которого равен "
           "абсолютному значению pid\n");
  }
  else if (children_pid == -1)
  {
    printf("waitpid: Ожидание любого дочернего процесса\n");
  }
  else if (children_pid == 0)
  {
    printf("waitpid: Ожидание любого дочернего процесса, "
           "идентификатор группы процессов которого равен "
           "идентификатору текущего процесса.\n");
  }
  else if (children_pid > 0)
  {
    printf("waitpid: Ожидание дочернего процесса, чей идентификатор равен "
           "pid=%d\n",
           sig_info->si_pid);
  }
}

int main(int argc, char const *argv[])
{
  struct sigaction act;
  sigset_t set;

  if (argc > 1)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  // Выделяем память под act
  memset(&act, 0, sizeof(act));

  // Устанавливаем обработчик (хендлер) к сигналу
  act.sa_sigaction = sig_hander;

  // Устанавливаем маску сигнала для обработки хендлером
  if (sigemptyset(&set) != 0)
  {
    fprintf(stderr, "Ошибка инициализации набора сигналов\n");
    return EXIT_FAILURE;
  }

  // Добавляем маску SIGCHLD к сету
  if (sigaddset(&set, SIGCHLD) != 0)
  {
    fprintf(stderr, "Ошибка добавления сигнала к сету\n");
    return EXIT_FAILURE;
  }

  // Добавим маску к сигналу
  act.sa_mask = set;

  // Прокидываем доп. инфу в хендлер
  act.sa_flags = SA_SIGINFO;

  // Слушаем сигнал SIGCHLD (изменение состояния процесса)
  if (sigaction(SIGCHLD, &act, 0) == -1)
  {
    fprintf(stderr, "Ошибка sigaction(%d)\n", SIGCHLD);
    return EXIT_FAILURE;
  }

  switch (fork())
  {
    case -1:
      fprintf(stderr, "Ошибка fork()\n");
      return EXIT_FAILURE;

    case 0:
      printf("Внутри дочернего процесса pid=%d...\n", getpid());
      break;

    default:
      printf("Внутри основного процесса pid=%d, ждем возврата из pause()...\n",
            getpid());
      // Приостановим выполнения текущего процесса до получения сигнала
      pause();
      break;
  }

  return 0;
}
