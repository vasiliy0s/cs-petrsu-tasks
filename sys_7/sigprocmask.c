/**
 * sigprocmask.c -- утилита для блокировки/разблокировки
 * получения маски
 *
 * Copyright (c) <2018> <vinograd@kappa.cs.petrsu.ru>
 *
 * This code is licensed under a MIT-style license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

// Пример использования программы храним в ОДНОМ месте
#define USAGE "Использование: ./sigprocmask"

void sig_handler(int sig);
int sig_send(int sig, char *);

int main(int argc, char const *argv[])
{
  sigset_t new_set;
  sigset_t exist_set;
  struct sigaction act;

  if (argc > 1)
  {
    fprintf(stderr, "Неверный вызов программы\n");
    fprintf(stderr, "%s\n", USAGE);
    return EXIT_FAILURE;
  }

  memset(&act, 0, sizeof(act));

  // Устанавливаем маску сигнала для обработки хендлером
  if (sigemptyset(&new_set) != 0)
  {
    fprintf(stderr, "Ошибка инициализации набора сигналов\n");
    return EXIT_FAILURE;
  }

  // Добавляем маску SIGUSR1 к сету обработки
  if (sigaddset(&new_set, SIGUSR1) != 0)
  {
    fprintf(stderr, "Ошибка добавления сигнала к сету\n");
    return EXIT_FAILURE;
  }

  act.sa_flags = 0;
  act.sa_mask = new_set;
  act.sa_handler = sig_handler;

  // Устанавливаем обработчик сигнала SIGUSR1
  if (sigaction(SIGUSR1, &act, NULL) == -1)
  {
    fprintf(stderr, "Ошибка sigaction(%d)\n", SIGUSR1);
    return EXIT_FAILURE;
  }

  printf("Отправляем сигналы в процессе с pid=%d\n", getpid());

  // Проверяем, что потомок может получать SIGUSR1 изначально
  if (sig_send(SIGUSR1, "до блокировки SIGUSR1") != 0)
  {
    fprintf(stderr, "Ошибка sig_send(%d)\n", SIGUSR1);
    return EXIT_FAILURE;
  }

  // Блокируем получение маски сигнала через флаг SIG_BLOCK
  if (sigprocmask(SIG_BLOCK, &new_set, &exist_set) < 0)
  {
    fprintf(stderr, "Ошибка sigprocmask(%d)\n", SIG_BLOCK);
    return EXIT_FAILURE;
  }

  // Проверяем, что потомок не получает сигнал
  if (sig_send(SIGUSR1, "после блокировки SIGUSR1") != 0)
  {
    fprintf(stderr, "Ошибка sig_send(%d)\n", SIGUSR1);
    return EXIT_FAILURE;
  }

  // Разблокируем SIGUSR1 через установку изначальной маски
  if (sigprocmask(SIG_SETMASK, &exist_set, NULL) < 0)
  {
    fprintf(stderr, "Ошибка sigprocmask(%d)\n", SIG_BLOCK);
    return EXIT_FAILURE;
  }

  // Проверяем, что потомок СНОВА не получает сигнал
  if (sig_send(SIGUSR1, "после восстановления изначальной маски"))
  {
    fprintf(stderr, "Ошибка sig_send(%d)\n", SIGUSR1);
    return EXIT_FAILURE;
  }

  return 0;
}

// Хендлер сигнала
void sig_handler(int sig)
{
  printf("--> Получен сигнал %d в pid=%d!\n", sig, getpid());
}

// Функция отправки сигнала
int sig_send(int sig, char * info)
{
  int result;

  printf("\nПосылаем сигнал %d %s...\n", sig, info);
  result = raise(sig);
  printf("Сигнал должен был быть получен...\n");
  sleep(1);

  return result;
}
