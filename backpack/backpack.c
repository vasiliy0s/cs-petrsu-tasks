#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// weight = items[n][0]
// cost = items[n][1]

int max_backpack_cost(int w, int n, int items[n][2]);

int main()
{
  FILE * input;
  FILE * output;

  // Пытаемся прочесть файл
  if ((input = fopen("input.txt", "r")) < 0)
  {
    fprintf(stderr, "Невозможно считать 'input.txt'");
    return EXIT_FAILURE;
  }

  // Считываем w(вес), n(кол-во предметов в списке)
  int w = 0, n = 0;
  fscanf(input, "%d %d", &w, &n);

  // Считываем вещи из рюкзака
  int items[n][2];
  for (int i = 0; i < n; i++)
  {
    fscanf(input, "%d %d", &items[i][0], &items[i][1]);
  }

  // Закрываем файл
  fclose(input);

  // Выполняем расчет
  int response = max_backpack_cost(w, n, items);

  // Записываем результат в "output.txt"
  if ((output = fopen("output.txt", "w+")) < 0)
  {
    fprintf(stderr, "Невозможно создать 'output.txt' с ответом");
    return EXIT_FAILURE;
  }
  fprintf(output, "%d", response);
  fclose(output);

  return 0;
}

int max_backpack_cost(int w, int n, int items[n][2])
{
  // Максимальная ценность предметов
  int max_costs[n+1][w+1];

  for (int jj = 0; jj <= w; jj++)
  {
    max_costs[0][jj] = 0;
  }

  for (int ii = 1; ii <= n; ii++)
  {
    int item_weight = items[ii-1][0];
    int item_cost = items[ii-1][1];

    for (int jj = 0; jj <= w; jj++)
    {
      int ic = max_costs[ii - 1][jj];
      int prev_cost = 0;

      // printf("item_weight=%d %s jj=%d\t\t", item_weight, item_weight > jj ? ">" : "<=", jj);

      if (item_weight > jj)
      {
        max_costs[ii][jj] = ic;
      }
      else
      {
        prev_cost = max_costs[ii - 1][jj - item_weight];
        max_costs[ii][jj] = fmax(ic, prev_cost + item_cost);
      }
      // printf("max_costs[%d][%d]=%d\t\t ic=%d\t\t item_cost=%d\t\t prev_cost=%d\n", ii, jj, max_costs[ii][jj], ic, item_cost, prev_cost);
    }
  }

  return max_costs[n][w];
}
